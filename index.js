function task1(array, DOM = document.body){
    let arr = document.createElement('ul');
    array.forEach(item=>{
        const li = document.createElement('li');
        li.textContent = item;
        arr.appendChild(li);
    })
    DOM.appendChild(arr);
}
let h1 = document.querySelector('h1');
let arr = ['BMW', 'Mercedes', 'Audi', 'Ford'];
task1(arr, h1);